# DO NOT CHANGE THIS FILE
# run like this: pip3 install -r requirements.txt --upgrade

# 3rd party dependencies
pyserial>=3.1.1
Send2Trash>=1.3.0
python-dateutil
numpy
sip
pyqt5
qscintilla
matplotlib # for user plugins

# for mac os builds (only useful if you want to build Mac OS executables)
#py2app

# rtd dependencies (only useful if you want to write documentation)
#sphinx>=1.5.5
#sphinx_rtd_theme>=0.2.4
#recommonmark

# swp dependencies
https://github.com/UmSenhorQualquer/pyforms/archive/master.zip
https://bitbucket.org/fchampalimaud/logging-bootstrap/get/master.zip
https://bitbucket.org/fchampalimaud/pybranch/get/master.zip
https://github.com/UmSenhorQualquer/pyforms/archive/master.zip
https://bitbucket.org/fchampalimaud/pyforms-generic-editor/get/master.zip
https://bitbucket.org/fchampalimaud/pybpod-api/get/development.zip

# plugins
https://bitbucket.org/fchampalimaud/pybpod-gui-plugin/get/development.zip
https://bitbucket.org/fchampalimaud/pybpod-gui-plugin-timeline/get/master.zip
https://bitbucket.org/fchampalimaud/pybpod-gui-plugin-session-history/get/master.zip
git+https://UmSenhorQualquer@bitbucket.org/fchampalimaud/pybpod-rotary-encoder-module.git